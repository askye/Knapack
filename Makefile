knapsack: main.o maxVal.o
	gcc -o knapsack main.o maxVal.o
main.o: main.c main.h
	gcc -c main.c
maxVal.o: maxVal.c
	gcc -c maxVal.c

clean:
	rm knapsack main.o maxVal.o
