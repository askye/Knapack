//Amethyst Skye//CSE222//03-05-2021//
//Programming Assignment 5 - Knapsack//
//
//Function name: main
//
//Purpose: to read a specified capacity and data file from the user, and
//display the maximum value provided the specified capacity.
//
//Inputs: 
//# - will be used as the capacity of knapsack
//data file - must have a name, value, and weight. Is used in determining
//the maximum value of knapsack.
//
//Return: a print of the list of contents of data file with appropriate 
//labels for each column, the bags capacity, maximum value, and a list of 
//items which compose the maximum value knapsack.
//

#include "main.h"

int main(int argc, char *argv[])
{
  if(argc == 3){
  int cap = atoi(argv[1]); //save bad capacity
  int i=0, weight, value;//temp variables for parsing 
  char buffer[120], name[33];
  FILE *fp=fopen(argv[2],"r"); //read contents of data file
    if(cap < 1 || cap > 1024){
      printf("Illegal capacity: should be between 1 and 1024.\n");
      return(0);
    }
    if(fp == NULL){ //invalid file
      printf("Error loading file.\n");
      return(0);
    }
    else{ //valid file, continue
      while(fgets(buffer,120,fp) != NULL){ //assign values to arrays from file
        sscanf(buffer, "%s %d %d",name,&value,&weight); //save data
        items[i].name = malloc(sizeof(strlen(name))); //malloc for string (name)
        strcpy(items[i].name, name);
        items[i].value = value;
        items[i].weight = weight; 
        i++;
      }//end of while file loop
      itemCount = i; //save number of lines in data file   
      printf("Weight   Value   Name\n");
      printf("------   -----   ----\n");
        for(int j=0;j<i;j++){ //loop through data for display
          printf(" %4d    %4d   %s\n",items[j].weight,items[j].value,items[j].name);
        }
      printf("-------------------------\n"); 
      printf("Bag's capacity=%d\n",cap); //list bag capacity
      struct answer highV = maxVal(cap);
      int max = highV.maxVal; //save max val to print
      printf("Highest possible value=%d\n",max); //list max value
      for(int c=0;c<itemCount;c++){ //loop to print items in knapsack
        if(highV.items[c] != 0){
          printf("Item %d (%s): %d\n",c,items[c].name,highV.items[c]);
        }
      }
    }
  }//end of checking for argc == 3
  else{ 
  printf("Usage: knapack int databasefile\n");
  return(0);
  }
}//end of function
