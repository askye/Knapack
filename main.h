#include <stdio.h>
#include <string.h>
#include <stdlib.h>

//definition for data structure handling item data
struct item{
  int weight;
  int value;
  char *name;
};

//deinition for data structure handling maxVal function
struct answer{
  unsigned char items[128];
  int maxVal;
};

int itemCount; //serves as the number of items within data file
struct answer cache[1025]; //cache for maxVal function
struct item items[128];

//function definitions
struct answer maxVal(int);
