//Function name: maxVal
//
//Purpose: to determine the maximum value given a specified knapsack capacity.
//
//Inputs: 
//int cap - integer representing the capacity of the knapsack
//
//Return: 
//struct answer maxVal - a structure containing the number of specified items
//which contribute to the overall maximum value for knapsack. 
//The struct will also contain the maximum value possible for knapsack of 
//given capacity.
//

#include "main.h"

struct answer maxVal(int cap)
{
  if(cache[cap].maxVal != 0){ //base case
    return(cache[cap]);
  } 
  struct answer max = {0}; //struct used to return max value
  max.maxVal = 0;
    if(cap <= 0){ //upon reaching capacity
      cache[cap] = max;
      return(max); //return max value
    }
    for(int i=0;i<itemCount;i++){ //checking all items
      if(items[i].weight <= cap){ //if items weight is less than capacity, continue
        struct answer temp = maxVal(cap - items[i].weight); //recursion
        //if item is considered for knapsack
        if(temp.maxVal + items[i].value > max.maxVal){
          max=temp;
          max.maxVal = temp.maxVal + items[i].value;
          max.items[i]++;
        }
      }
    } //end of for loop
    cache[cap] = max;
    return(max);
}

